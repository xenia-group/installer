# Installer

The installation tool for Xenia Linux.

## Usage

Refer to [wiki](https://wiki.xenialinux.com/en/latest/installation/guided-install.html).